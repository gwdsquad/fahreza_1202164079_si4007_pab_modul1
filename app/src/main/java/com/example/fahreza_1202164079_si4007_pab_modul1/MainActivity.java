package com.example.fahreza_1202164079_si4007_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText panjang, lebar;
    TextView hasil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        panjang = findViewById(R.id.edPanjang);
        lebar = findViewById(R.id.edLebar);
        hasil = findViewById(R.id.txHasil);
    }

    public void calc(View view){
        hasil.setText("");
        if (panjang.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(this, "Isi panjang yang ingin cari", Toast.LENGTH_SHORT).show();
            panjang.requestFocus();
        }else if (lebar.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(this, "Isi lebar yang ingin cari", Toast.LENGTH_SHORT).show();
            lebar.requestFocus();
        }else {
            int p, l, luas;
            p = Integer.parseInt(panjang.getText().toString());
            l = Integer.parseInt(lebar.getText().toString());

            luas = p * l;

            hasil.setText("" + luas);

        }
    }



    }
